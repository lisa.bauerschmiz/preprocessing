import numpy as np
import matplotlib.pyplot
from sklearn import preprocessing
from sklearn.model_selection import train_test_split
import os
from load_quilt_data import load_data
import keras
from keras.optimizers import Adam
from keras.models import Sequential
from keras.layers import Convolution1D, MaxPooling1D, Flatten, BatchNormalization, Dense, SimpleRNN
from keras import metrics
from keras.activations import relu, sigmoid, softmax
from keras import backend as K
import numpy as np
from load_quilt_data import load_data
from keras.models import load_model
import sklearn.metrics as metrics
from visualise_data import plot_confusion_matrix
from keras.preprocessing import sequence
import numpy as np
import matplotlib.pyplot
from sklearn import preprocessing
from sklearn.model_selection import train_test_split





def normalise_data(X):
    #normalization
    final_ticks = []
    final_modules = []
    for xi2 in X:
        ticks = []
        modules = []
        for item in xi2:
            ticks.append(item[0])
            modules.append(item[2])
        ticks = np.reshape(ticks,(-1, 1))
        modules = np.reshape(modules,(-1, 1))

        min_max_scaler = preprocessing.MinMaxScaler()
        modules_minmax = min_max_scaler.fit_transform(modules)
        ticks_minmax = min_max_scaler.fit_transform(ticks)
        final_ticks.append(ticks_minmax)
        final_modules.append(modules_minmax)
        
    
    X_first = []
    for xi3, minmax_ticks, minmax_modules in zip(X, final_ticks, final_modules):
        stripes = []
        release_pushs = []
        for i in xi3:

                stripes.append(i[1])
                release_pushs.append(i[3])
        stripes = np.asarray(stripes)
        release_pushs = np.asarray(release_pushs)
        X_first_item = np.column_stack((minmax_ticks, stripes, minmax_modules, release_pushs))
            
        X_first.append(np.asarray(X_first_item))
    
    return(X_first)


def fit_input_data(clusters, labels):

    new_labels = []
    new_clusters = []
    n = 5
    for cluster, label in zip(clusters, labels):

        if len(cluster) > n-1:
                
            for i in range(0, len(cluster)-n-1):

                cl_array = cluster[i:i+n]
                new_clusters.append(cl_array)
                new_labels.append(label)
            
        else:
            
            cluster = cluster.tolist()
            for i in range(n-len(cluster)):
                cluster.append([-1,-1,-1,-1]) 


            cl_array = np.asarray(cluster)
            new_clusters.append(cl_array)
            new_labels.append(label)    
    new_clusters = np.asarray(new_clusters)
    new_labels = np.asarray(new_labels)
    
    return(new_clusters, new_labels)

def convert_labels(new_labels):

    label_list = []
    for label in new_labels:
        if label == "CAR_IN" or label == 'CAR_OUT':
            label_list.append(1)
        else:
            label_list.append(0)
        

    
    return(label_list)

def separate_data(X,Y):
    
    # seperate in trainings and test data set
    x_train, x_test, y_train, y_test = train_test_split( X, Y, test_size=0.2, shuffle = 1)
    print('Data shapes(x_train/test,y_train/test): ',np.shape(x_train), np.shape(x_test), np.shape(y_train), np.shape(y_test))

 
    return (x_train, x_test, y_train, y_test)

def prepare_cd_data(X, Y1, Y2):
    
    X_0 = normalise_data(X)
    y2 = convert_labels(Y1)
    X_1, Y = fit_input_data(X_0, y2)

    #x_train, x_test, y_train, y_test = separate_data(X_1,Y_2)

    #y_train= np.expand_dims(x_train, axis=2)
    #y_test = np.expand_dims(x_test, axis=2)
    return(X_1, Y)
    

#### for transforming Y and generating datasets with the keras timeseries generator ####
#### Y must be same length than X ?                                                 ####
def transform_Y(clusters, labels):
    
    new_labels = []
    for cluster, label in zip(clusters, labels):
        new_l = []
        length_cluster = len(cluster)
        for i in range(length_cluster):
            new_l.append(label)
            new_array = np.asarray(new_l)
            
        new_labels.append(new_array)
    new_labels = np.asarray(new_labels)
        
    return(new_labels)

def data_gen(data, targets):

    data_gen = TimeseriesGenerator(data, targets,
                               length=5, batch_size = 10)
    
    
    assert len(data_gen) == 20

    batch_0 = data_gen[0]
    x, y = batch_0
    assert np.array_equal(x,
                      np.array([[[0], [2], [4], [6], [8]],
                                [[1], [3], [5], [7], [9]]]))
    assert np.array_equal(y,
                      np.array([[10], [11]]))

node = 'videos'
typ = 'clusters'

X, Y1, Y2, meta = load_data(node, typ)

X_0 = normalise_data(X)
y2 = convert_labels(Y2)
X_1, Y_2 = fit_input_data(X_0, y2)

x_train, x_test, y_train, y_test = separate_data(X_1,Y_2)