import numpy as np
import matplotlib.pyplot
from sklearn import preprocessing
from sklearn.model_selection import train_test_split

node = 'videos'
typ = 'clusters'


def normalise_data(X):
    #normalization
    final_ticks = []
    final_modules = []
    for xi2 in X:
        ticks = []
        modules = []
        for item in xi2:
            ticks.append(item[0])
            modules.append(item[2])
        ticks = np.reshape(ticks,(-1, 1))
        modules = np.reshape(modules,(-1, 1))

        min_max_scaler = preprocessing.MinMaxScaler()
        modules_minmax = min_max_scaler.fit_transform(modules)
        ticks_minmax = min_max_scaler.fit_transform(ticks)
        final_ticks.append(ticks_minmax)
        final_modules.append(modules_minmax)
        
    
    X_first = []
    for xi3, minmax_ticks, minmax_modules in zip(X, final_ticks, final_modules):
        stripes = []
        release_pushs = []
        for i in xi3:

                stripes.append(i[1])
                release_pushs.append(i[3])
        stripes = np.asarray(stripes)
        release_pushs = np.asarray(release_pushs)
        X_first_item = np.column_stack((minmax_ticks, stripes, minmax_modules, release_pushs ))
            
        X_first.append(X_first_item)
    
    return(X_first)

def fit_input_data(X, Y1, Y2):
    # append -1 if length not 61
    X_new = []
    Y_new = []
    for xi, yi, y2i in zip(X, Y1, Y2):

        if yi == 'CAR_IN' or yi == 'CAR_OUT':

            if len(xi) != 61:
                xi = xi.tolist()
                for i in range(61-len(xi)):
                    xi.append([-1,-1,-1,-1])

            x_new = np.asarray(xi)
            X_new.append(x_new)
            Y_new.append(0)  

        if yi == 'MOTORCYCLE' or yi == 'PEDESTRIAN':

            if len(xi) != 61:
                xi = xi.tolist()
                for i in range(61-len(xi)):
                    xi.append([-1,-1,-1,-1])

            x_new = np.asarray(xi)
            X_new.append(x_new)
            Y_new.append(1) 

        
          
            
    # binary encode: 1000 = car in, 0100 = car out, 0010 = motorcycle, 0001 = bicycle
    
    Y_new = np.asarray(Y_new)
    onehot_encoder = preprocessing.OneHotEncoder(sparse=False)
    Y_new_shape = Y_new.reshape(len(Y_new), 1)
    onehot_encoded = onehot_encoder.fit_transform(Y_new_shape)            
    X_new = np.asarray(X_new)

    return(X_new, onehot_encoded)

def separate_data(X,Y):
    
    # seperate in trainings and test data set
    x_train, x_test, y_train, y_test = train_test_split( X, Y, test_size=0.2, shuffle = 1)
    print('Data shapes(x_train/test,y_train/test): ',np.shape(x_train), np.shape(x_test), np.shape(y_train), np.shape(y_test))

 
    return (x_train, x_test, y_train, y_test)

def prepare_cn_data(X, Y1, Y2):
    
    X_0 = normalise_data(X)
    X_1, Y = fit_input_data(X_0, Y1, Y2)
    #x_train, x_test, y_train, y_test = separate_data(X_1,Y)
    
    return(X_1, Y)
    

def shuffle_data(X, Y):
    dictionary = dict(zip(X,Y))
    keys =  list(dictionary.keys()) 
    random.shuffle(keys)
    X_new = []
    for key, value in dictionary.items():
        X_new.append(key)
        Y_new.append(value)
    return(X_new, Y_new)

