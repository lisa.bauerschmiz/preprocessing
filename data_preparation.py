import numpy as np
import matplotlib.pyplot
from sklearn import preprocessing

node = 'videos'
typ = 'clusters'


def normalise_data(X):
    #normalization
    final_ticks = []
    final_modules = []
    for xi2 in X:
        ticks = []
        modules = []
        for item in xi2:
            ticks.append(item[0])
            modules.append(item[2])
        ticks = np.reshape(ticks,(-1, 1))
        modules = np.reshape(modules,(-1, 1))

        min_max_scaler = preprocessing.MinMaxScaler()
        modules_minmax = min_max_scaler.fit_transform(modules)
        ticks_minmax = min_max_scaler.fit_transform(ticks)
        final_ticks.append(ticks_minmax)
        final_modules.append(modules_minmax)
        
    
    X_first = []
    for xi3, minmax_ticks, minmax_modules in zip(X, final_ticks, final_modules):
        stripes = []
        release_pushs = []
        for i in xi3:
                #import pdb; pdb.set_trace()

                stripes.append(i[1])
                release_pushs.append(i[3])
        stripes = np.asarray(stripes)
        release_pushs = np.asarray(release_pushs)
        X_first_item = np.column_stack((minmax_ticks, stripes, minmax_modules, release_pushs ))
            
        X_first.append(X_first_item)
    
    return(X_first)

def fit_input_data(X, Y1, Y2):
    # append -1 if length not 61
    X_new = []
    Y_new = []
    for xi, yi, y2i in zip(X, Y1, Y2):
        
        if yi == 'CAR_IN':
        
            if len(xi) != 61:
                xi = xi.tolist()
                for i in range(61-len(xi)):
                    xi.append([-1,-1,-1,-1])
            x_new = np.asarray(xi)
            X_new.append(x_new)
            Y_new.append(0)
            
        elif yi == 'CAR_OUT':
        
            if len(xi) != 61:
                xi = xi.tolist()
                for i in range(61-len(xi)):
                    xi.append([-1,-1,-1,-1])
            x_new = np.asarray(xi)
            X_new.append(x_new)
            Y_new.append(1)

    X_new = np.asarray(X_new)
    Y_new = np.asarray(Y_new)
    return(X_new, Y_new)

def separate_data(X,Y):
    
    # seperate in trainings and test data set
    x_train = X[:-400]
    x_test = X[-400:]
    y_train = Y[:-400]
    y_test = Y[-400:]
    print('Data shapes(x_train/test,y_train/test): ',np.shape(x_train), np.shape(x_test), np.shape(y_train), np.shape(y_test))

 
    return (x_train, x_test, y_train, y_test)

def prepare_data(X, Y1, Y2):
    
    X_0 = normalise_data(X)
    X_1, Y = fit_input_data(X_0, Y1, Y2)
    x_train, x_test, y_train, y_test = separate_data(X_1,Y)
    
    return(x_train, x_test, y_train, y_test)
    